# <img src="https://gitlab.com/distant-horizons-team/distant-horizons-core/-/raw/main/_Misc%20Files/logo%20files/new/SVG/Distant-Horizons-Core.svg" height="128px">

This repo is for the Distant Horizons mod.
The purpose of this submodule is to isolate code that isn't tied to a specific version of minecraft. This prevents us from having duplicate code; reducing errors and helping us port to different versions faster and easier.

Check out the mod's main GitLab page here:
https://gitlab.com/distant-horizons-team/distant-horizons

## source code installation

You shouldn't download this repo directly. 
It should be automatically included when pulling the full mod.


## Open Source Acknowledgements

LZ4 for Java (data compression)\
https://github.com/lz4/lz4-java

NightConfig for Json & Toml (config handling)\
https://github.com/TheElectronWill/night-config

SVG Salamander for SVG support (not being used atm)\
https://github.com/blackears/svgSalamander

sqlite-jdbc\
https://github.com/xerial/sqlite-jdbc
